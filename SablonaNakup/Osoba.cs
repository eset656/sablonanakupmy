﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SablonaNakup
{

    public struct Smer
    {

        private sbyte x, y;

        public Smer(sbyte x, sbyte y)
        {
            this.x = (sbyte)Math.Sign(x);
            this.y = (sbyte)Math.Sign(y);
        }

        public sbyte X {
            get
            {
                return x;
            }
            set
            {
                x = (sbyte)Math.Sign(value);
            }
        }
        public sbyte Y
        {
            get
            {
                return y;
            }
            set
            {
                y = (sbyte)Math.Sign(value);
            }
        }
    }

    abstract class Osoba
    {
        /// <summary>
        /// Generátor náhodných čísel, který můžete používat.
        /// Nepoužívejte vlastní vytvořený generátor (new Random). Vysvětlení buď vygooglete nebo se na něj zeptejte.
        /// </summary>
        protected Random nahoda;

        public Random Nahoda { set => nahoda = value; }
        

    }

    abstract class Nakupujici : Osoba
    {
        abstract public Smer Krok(char[,] castMapy, int penize);
    }

    abstract class Zlodej : Osoba
    {
        abstract public Smer Krok(char[,] castMapy, int penize, Souradnice mojeSouradnice);
    }
}
